♠# Installer Node.js en version 15.3.0
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
nvm install 15.3.0

# Créer les 3 dossiers
touch staging -d
touch production -d
touch pre-production -d

if [ -z "$1" && $# < 2 ]
	then
      		# Créer et lancer un serveur node qui repond  "Hello $environnement" en fonction du path de $environnement passé par l'url

		# 1. Récupérer le path de l'url dans la variable $environnement
		export environnement="$1"
		if [ $environnement != "staging" && $environnement != "production" && $environnement != "pre-production" ]
			then
				echo "Erreur: Nom d'environnement incorrect !"
			else
				# 2. Créer et lancer le serveur node qui répond "Hello $environnement"
				echo "Hello $environnement !"
				node $environnement.js
		fi
	elif [ -z "$1" && $# > 1 ]
		echo "Erreur: Un seul argument peut être saisi pour ce script !"

fi
