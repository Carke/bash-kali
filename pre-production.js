const express = require('express')
const app = express()
const port = 3003

const environnement = 'pre-production'
const file =  '/$(pwd)/pre-production/sentence.ini'

fs = require('fs');
let sentence = fs.readFile(file, 'utf8');

app.get('/' + environnement + '/', (req, res) => {
  res.send(sentence)
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
