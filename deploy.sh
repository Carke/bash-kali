# Récupère deux arguments
if [ -z "$1" ]
	then
      		echo "Erreur: premier argument vide !"
	else

      		if [ -z "$2" ]
			then
      				echo "Erreur: second argument vide !"
			else
				export $arg1 = "$1"
				export $arg2 = "$2"
      				if [ -z "$arg1" != "staging" && "$arg2" != "pre-production" && "$arg3" != "production" ]
					then
      						echo "Erreur: Environnement invalide !"
					else
      						if [ -z grep([:space:], "$arg2") ]
							# Modifie le contenu du serveur web en fonction des arguments
							then
								cd $(pwd)/$arg1
								touch sentence.ini
								echo $arg2 > sentence.ini
      								setup.sh $arg1
							else
								echo "Erreur: second argument incorrect !"
						fi
				fi
		fi
fi


